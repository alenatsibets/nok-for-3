import java.util.Scanner;

public class Nok_for_3 {
    public static void main(String[] args) {
        int a, b, c, nok1, nod1, nod2, nok2;
        a = inputIntFromConsole("Enter the first value: ");
        b = inputIntFromConsole("Enter the second value: ");
        c = inputIntFromConsole("Enter the third value: ");
        nod1 = nod(a, b);
        nok1 = nok(a, b, nod1);
        nod2 = nod(nok1, c);
        nok2 = nok(nok1, c, nod2);
        display(nok2);
    }
    public static int inputIntFromConsole(String message) {
        int value;
        Scanner sc = new Scanner(System.in);
        System.out.print(message);
        while (!sc.hasNextInt()) {
            sc.nextLine();
            System.out.print("Incorrect value. " + message);
        }
        value = sc.nextInt();
        return value;
    }
    public static int nod(int a, int b) {
        while (a != 0 && b != 0) {
            if (a > b) { a = a % b; }
            else { b = b % a; }
        }
        return a + b;
    }
    public static int nok(int x, int y, int nod) {
        return x*y/nod;
    }
    public static void display(int nok){
        System.out.println("The smallest common multiple: " + nok);
    }
}